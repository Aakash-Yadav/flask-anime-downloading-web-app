from flask import (Flask,request,redirect,render_template,url_for,session,flash)
from os import urandom
from main import Data_post,Data_post_second
from random import choice
from base import data

################## app
app = Flask(__name__)
app.config['SECRET_KEY'] = '%s%s%s'%(urandom(50),urandom(50),urandom(50))

names = sorted(data.keys())
global new_data
#### main
@app.route('/')
@app.route('/Home')
def Home():
    value = [choice(names) for x in range(100+1)]
    dec_value = [data[x] for x in value]
    return render_template('home.html',data=dec_value)


@app.route('/search',methods=['GET','POST'])
def Search():
    if request.method == 'POST':

        session['Anime_name'] = request.form.get('somthing')
        return redirect(url_for('Information'))
    return render_template('index.html')

@app.route('/search_define/<n>',methods=['GET','POST'])
def Search_By_name(n):    
    session['Anime_name'] = n
    return redirect(url_for('Information'))


@app.route('/information',methods=['GET','POST'])
def Information():
    try:
        val =   Data_post(session['Anime_name'])
       
        new_data= {
        'image':val['image'],
        'summary':val['Description'],
        'total_ep':val['Total_ep'],
        'name_of_anime':val['name']
    }
        session['total_ep_'] = new_data['total_ep']
        session['name_watching_info'] = new_data['name_of_anime']
    except:
        return redirect(url_for('Search'))
    basic_data = new_data

    return render_template('download.html',img=basic_data['image'],name=basic_data['name_of_anime'].upper() ,pera=basic_data['summary'],Tep=basic_data['total_ep']
    )

@app.route('/Download-ep',methods=['GET','POST'])
def Download_ep():
    try:
        two_flash = False
        ep =   session['total_ep_']
        cheak = Data_post(session['Anime_name'])['episode_links']
    except:
        return redirect(url_for('Search'))
    

    if request.method == 'POST':
        session['ep'] = request.form.get('user')
        try:
            out=cheak[(session['ep'])]
            if out=='None' or out==None:
                flash(f'sorry link not found for episode {session["ep"]}',category='error')
            else:
                return redirect(out)
        except:
            flash(f'not able to find link for {session["ep"]}',category='error')
            return redirect(url_for('Download_ep'))
    return render_template('last.html',num=ep,two_flash=two_flash)


@app.route('/watch_episode',methods=['GET','POST'])
def watch_episode_anime():
    try:
        get_anime_data = Data_post_second(session['name_watching_info'])
        
        values = {
            'total_ep':len(get_anime_data),
            'server_links':get_anime_data
        }
    except:
        return redirect('Information')
    if request.method == 'POST':
        epsode = request.form.get('user')
        session['data_of_epsode_of_uyser_data'] = epsode
        session['data_of_live_server'] = values['server_links'].get(epsode)
        
        try:
            check =session['data_of_live_server']
            if check=='None' or check==None:
                flash(f'sorry link not found for episode {session["ep"]}',category='error')
            else:
                return redirect(url_for('server_links'))
        except:
            flash(f'not able to find link for {epsode}',category='error')
            return redirect(url_for('watch_episode_anime'))
            
    return render_template('form_for_watch_episode.html',num=values['total_ep'])

@app.route('/server')
def server_links():
    try:
            
        links = session['data_of_live_server'].split('<$>')
        link1 = ['https://dood.ws/e/%s'%(links[0]),'https://fembed-hd.com/v/%s'%(links[-1])]
        loop_data = [x for x in range(len(link1))]
        emoji = ['⚡','🔥']
        return render_template('server.html',data=link1,loop=loop_data,emo=emoji)
    except:
        return redirect(url_for('Information'))


if __name__ == '__main__':
    app.run(debug=1)




