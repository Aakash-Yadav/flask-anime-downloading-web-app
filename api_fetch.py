from requests import post
from functools import lru_cache
from concurrent.futures import ThreadPoolExecutor
from collections import ChainMap
from bs4 import BeautifulSoup


@lru_cache(maxsize=500)
def get_data_from_server(n:str):
    url = post('************************',data={
        'name':n
    })
    return url.json()

@lru_cache(maxsize=500)
def get_data_from_server_watching_links(n:str):
    url = post('*************************',data={
        'name_watching':n
    })
    return url.json()


@lru_cache(maxsize=500)
def Data_post(val):
    exe = ThreadPoolExecutor()
    data=(exe.map(get_data_from_server,[val]))
    return (dict(ChainMap(*[x for x in data])))


@lru_cache(maxsize=500)
def Data_post_second(val):
    exe = ThreadPoolExecutor()
    data=(exe.map(get_data_from_server_watching_links,[val]))
    return (dict(ChainMap(*[x for x in data])))