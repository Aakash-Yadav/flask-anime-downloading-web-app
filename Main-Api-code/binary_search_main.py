from bisect import bisect
from collections import deque
from functools import lru_cache
from index8 import name as data
from online_links import all_links


a = (deque(data.keys()))
b = (deque(data.values()))

@lru_cache(maxsize=150)
def search_anime(n):
    # b = sorted(b)
    return  b[(bisect((a),(n).lower()))-1]

@lru_cache(maxsize=150)
def search_anime_watching(n):
    # b = sorted(b)
    return  all_links[n]

