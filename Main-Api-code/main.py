from flask import Flask , jsonify,request,session,redirect
from os import urandom
from binary_search_main import search_anime ,search_anime_watching


app = Flask(__name__)
app.config['SECRET_KEY'] = '%s'%(urandom(50))


@app.route('/',methods=['GET','POST'])
def Home():
    if request.method == 'POST':
        session['name_of_anime'] = request.form.get('name')
        value =  search_anime(session['name_of_anime'])
        return jsonify(value)
    return redirect('https://rb.gy/k63c4a')



@app.route('/watching',methods=['GET','POST'])
def Watching():
    if request.method == 'POST':
        name_of_anime_watching = request.form.get('name_watching')
        value =  search_anime_watching(name_of_anime_watching)
        return jsonify(value)
    return redirect('https://rb.gy/k63c4a')





if __name__ == '__main__':
    app.run()


