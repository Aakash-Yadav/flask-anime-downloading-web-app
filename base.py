data = {
  " 3d kanojo: real girl": {
    "name": " 3d kanojo: real girl",
    "image": "https://gogocdn.net/cover/3d-kanojo-real-girl.png",
    "Description": "Tsutsui Hikari is an otaku, and he mostly avoids social life. Hikari has only one friend at school, who is also a social misfit, and he is mocked brutally by most of his classmates for being creepy and weird. One day, he ends up having to clean the school pool with Igarashi Iroha, who appears to be pretty much everything he hates in real-life girls. She skips school, has a blunt manner, doesn't have female friends, and seems the sort to be promiscuous. However, she is friendly to Hikari, and even stands up to the people who make fun of him. Hikari's bitterness and trust issues lead him to say pretty harsh things to Iroha, but she never dismisses him as creepy. After a while, it starts to look like Iroha may become his first real-life, 3D girlfriend! Will he be able to handle it?"
  },
  " devils line": {
    "name": " devils line",
    "image": "https://gogocdn.net/cover/devils-line.png",
    "Description": "Anzai, half vampire, and Tsukasa, a normal school girl.\r\n\r\nVampires seem to be living among humans. Of course the government does not know of their existence, because their appearance does not differ from humans. They also do not need to drink blood, but when they get a craving or get angry, they can become uncontrollable monsters."
  },
  " generator rex season 02 (dub)": {
    "name": " generator rex season 02 (dub)",
    "image": "https://gogocdn.net/cover/generator-rex-season-02-dub.png",
    "Description": "A 15 year old boy with the ability to produce machines from his body via nanites is enlisted by the government to stop other creatures created by the same science."
  },
  " generator rex season 03 (dub)": {
    "name": " generator rex season 03 (dub)",
    "image": "https://gogocdn.net/cover/generator-rex-season-03-dub.png",
    "Description": "A 15 year old boy with the ability to produce machines from his body via nanites is enlisted by the government to stop other creatures created by the same science."
  },
  " kujira no kora wa sajou ni utau (dub)": {
    "name": " kujira no kora wa sajou ni utau (dub)",
    "image": "https://gogocdn.net/cover/kujira-no-kora-wa-sajou-ni-utau-dub.png",
    "Description": "In a world covered by an endless sea of sand, there sails an island known as the Mud Whale. In its interior lies an ancient town, where the majority of its inhabitants are said to be \"Marked,\" a double-edged trait that grants them supernatural abilities at the cost of an untimely death. Chakuro is the village archivist; young and curious, he spends his time documenting the discovery of newfound islands. But each one is like the rest\u2014abandoned save for the remnants of those who lived there long ago. \r\n\r\nFor the first time in six months, another island crosses the horizon, so Chakuro and his friends join the scouting group. During the expedition, they find vestiges of an archaic civilization. And inside one of its crumbling remains, Chakuro discovers a girl who will change his destiny and the world inside the Mud Whale as he knows it."
  },
  " kung fu panda (dub)": {
    "name": " kung fu panda (dub)",
    "image": "https://gogocdn.net/cover/kung-fu-panda-dub.png",
    "Description": "In the Valley of Peace, Po the Panda finds himself chosen as the Dragon Warrior despite the fact that he is obese and a complete novice at martial arts."
  },
  " macross delta movie: gekijou no walk\u00fcre": {
    "name": " macross delta movie: gekijou no walk\u00fcre",
    "image": "https://gogocdn.net/cover/macross-delta-movie-gekijou-no-walkure.png",
    "Description": "No synopsis information has been added to this title. Help improve our database by adding a synopsis here.\r\n"
  },
  " monsters vs. aliens (dub)": {
    "name": " monsters vs. aliens (dub)",
    "image": "https://gogocdn.net/cover/monsters-vs-aliens.png",
    "Description": "A woman transformed into a giant after she is struck by a meteorite on her wedding day becomes part of a team of monsters sent in by the U.S. government to defeat an alien mastermind trying to take over Earth."
  },
  " transformers: war for cybertron season 3 (dub)": {
    "name": " transformers: war for cybertron season 3 (dub)",
    "image": "https://gogocdn.net/cover/transformers-war-for-cybertron-season-3-dub.png",
    "Description": "Animated series set in the world of battling Autobots and Decepticons."
  },
  "\"eiyuu\" kaitai": {
    "name": "\"eiyuu\" kaitai",
    "image": "https://gogocdn.net/cover/eiyuu-kaitai.png",
    "Description": "Uro Yamada works as part of the \"Hero Retirement and Return to Everyday Life Assistance Agency,\" which takes heroes from another world back to Earth and helps them reintegrate into everyday life."
  },
  ".hack//g.u. returner": {
    "name": ".hack//g.u. returner",
    "image": "https://gogocdn.net/images/anime/5745.jpg",
    "Description": "hack//G.U. Returner is a single-episode OVA offered to fans who completed all three GU Games, featuring characters from the .hack//G.U. Games and .hack//Roots."
  },
  ".hack//g.u. trilogy": {
    "name": ".hack//g.u. trilogy",
    "image": "https://gogocdn.net/images/Trilogy.jpg",
    "Description": "Based on the CyberConnect2 HIT GAME, now will be released in a CG Movie!\r\n\r\nThe Movie will be placed in the storyline of each .hack//G.U. games trilogy. The story follows Haseo, a player in the online MMORPG called The World:R2 at first depicted as a PKK (Player Killer Killer) known as the \"Terror of Death\", a former member of the disbanded Twilight Brigade guild. Haseo encounters Azure Kite (believing him to be Tri-Edge and blaming him for what happened to Shino) but is hopelessly outmatched. Azure Kite easily defeats Haseo and Data Drains him, reducing his level from 133 to 1 and leaving him without any items, weapons, or member addresses. He is left with a mystery on his hands as to the nature of the Data Drain and why Azure Kite is in possession of such a skill.\r\n\r\nEventually Haseo gains the \"Avatar\" of Skeith. Acquiring the ability to call Skeith and wield his abilities, such as Data Drain. With Skeith as his strength, Haseo begins the quest for a way to save Shino.\r\n\r\nHe is seen seeking out a PK (Player Killer) known as Tri-Edge, whose victims supposedly are unable to return to The World after he PKs them. Haseo's friend, Shino, was attacked six months prior to the events of the game by Tri-Edge, and the player herself, Shino Nanao, was left in a coma.\r\n"
  },
  ".hack//gift": {
    "name": ".hack//gift",
    "image": "https://gogocdn.net/cover/hackgift.png",
    "Description": "As an expression of gratitude for the heroes of both the \".hack//Sign\" and the \".hack\" game series, Helba has prepared a special event for all the characters to find the newly established \"Twilight Hot Springs.\" The characters can get their well deserved rest and relaxation by having a soak in the wonderful hot springs, but there is only one problem\u2014the hot springs are hidden and there have been mysterious player murders. With the only clue being the word \u201cGIFT,\" the race has begun to find the culprit and the location of the hot springs.\r\n"
  },
  ".hack//legend of the twilight": {
    "name": ".hack//legend of the twilight",
    "image": "https://gogocdn.net/images/anime/hacklegend-of-the-twilight2.jpg",
    "Description": ".hack//Legend of the Twilight"
  },
  ".hack//liminality": {
    "name": ".hack//liminality",
    "image": "https://gogocdn.net/images/anime/A/hackliminality.jpg",
    "Description": "The story follows three girls, Mai Minase, Yuki Aihara, and Kyoko Tohno, and a former CC Corp. director of the Japanese version of The World, Junichiro Tokuoka. They investigate as to why several people have fallen into a coma while playing the game. The amine starts with the collapse of Mai and her boyfriend, (Tomonari Kasumi). It is soon revealed that she heard a certain sound (which is also present at important points in .hack//Sign) shortly before collapsing. She later wakes up to the same sound, the sound that tunes. \u201cA In C Minor\u201d.\r\n\r\nThrough the knowledge of all the the characters, the constantly search for the answer to why there have been so many victims due to \u201cThe World\u201d. Will they be able to battle something that doesn\u2019t even exist in the real world?\r\nGenre: Action/Science Fiction"
  },
  ".hack//liminality (dub)": {
    "name": ".hack//liminality (dub)",
    "image": "https://gogocdn.net/cover/hackliminality-dub.png",
    "Description": "While playing the newly released MMORPG \"The World,\" Minase Mai and Tomonari Kasumi collapsed in the real world and were sent to the hospital after hearing a mysterious sound. Only Mai had recovered while her friend Kasumi had slipped into a mysterious coma. Mai is later approached by the creator of \"The World,\" Tokuoka Junichiro, who unveils to her that, just like her friend, six other players across the country had also collapsed while playing \"The World\" and are now in mysterious comas. What exactly is \"The World,\" what is this mysterious sound Mai keeps on hearing, and why are people slipping into comas from this game? Minase, Junichiro and their acquaintances set off to get to the bottom of this problem before any further harm happens."
  },
  ".hack//quantum ": {
    "name": ".hack//quantum ",
    "image": "https://gogocdn.net/images/anime/A/hackquantum.jpg",
    "Description": "Plot Summary: Tobias, Mary, and Sakuya challenge the impregnable \u201cThe One Sin\u201d, but they lose their way in the maze and unintentionally trap other guild members. A mysterious cat is watching their blunde"
  },
  ".hack//roots": {
    "name": ".hack//roots",
    "image": "https://gogocdn.net/images/anime/A/hackroots.jpg",
    "Description": "hack//Roots is the prequel to the .hack// videogames. It follows the story of Haseo a member of the Twilight Brigade guild. In the year 2015 the C.C. Corp\u2019s building was burned down, and with it most of the data for \u201cThe World\u201d was lost. By mixing the remaining data with that of another game, the C.C. Corp was able to create the game, \u201cThe World: R2?, unlike the games before this game allowed player versus player combat. The anime starts when Haseo logs into The World R:2 for the first time and falls victim to the PKers that reside within. He is saved by Ovan, which prompts him to join the Twilight Brigade.\r\n\r\nThe Twilight Brigade members are on a mission to find the Key of the Twilight, however, the TAN guild opposes. Their current mission is to find the 6 virus cores before TAN does, and Haseo has 2 of them, how will he escape from the TAN Guild, and find the Key of Twilight, which might be the answer to everything.\r\nGenre: Action/Fantasy"
  },
  ".hack//roots (dub)": {
    "name": ".hack//roots (dub)",
    "image": "https://gogocdn.net/cover/hackroots-dub.jpg",
    "Description": "After the destruction of \"The World\" in 2015, CC Corporation rebuilt the game using data from what was previously to be another game. \"The World R:2\" was then released in 2016. A newcomer to the game, Haseo, is instantly PKed and then revived by a mysterious man known as Ovan. With the problem of PKs occupying the game, Haseo soon after receives help from a female Harvest known as Shino. Amidst curiosity and confusion, Haseo is lead to joining the guild known as the Twilight Brigade. The guild that searches for the legendary object known as the \"Key of the Twilight\". Set before the .hack//G.U. game."
  },
  ".hack//sign": {
    "name": ".hack//sign",
    "image": "https://gogocdn.net/images/anime/A/hacksign.jpg",
    "Description": "Taking place 2 years after the \u201cPluto\u2019s Kiss\u201d crisis. \u201cThe World\u201d once again returns. This time being the only PC game in existence. Tsubasa a boy who started playing only a few weeks before, finds himself taken to a mysterious zone, where he meets Aura, Morganna, and a mysterious player that has the appearance of a cat. Soon after realizing that he can\u2019t logout, he is forced to stay in \u201cThe World\u201d, but unlike other characters he can still feel pain, he still has his emotions.\r\n\r\nHe slowly separates himself from those who try to help him, the only ones that can help him. His friends Bear and Black Rose, constantly try to comfort him in his struggle, but he always pushes them away. He only wants to search for the answer to why he can\u2019t logout, and to the real question of who he really is.\r\nGenre: Adventure/Mystery/Fantasy"
  },
  ".hack//sign (dub)": {
    "name": ".hack//sign (dub)",
    "image": "https://gogocdn.net/cover/hacksign-dub.jpg",
    "Description": "A young wavemaster, only known by the alias of Tsukasa, wakes up in an MMORPG called The World, with slight amnesia. He does not know what he has previously done before he woke up. In The World, he is suspected to be a hacker of the Crimson Knights, as he was seen accompanying a tweaked character in the form of a cat. Unable to log out from the game, he wanders around looking for answers, avoiding the knights and other players he meets along the way.\r\n\r\nAs Tsukasa explores The World, he stumbles upon a magical item that takes the form of a 'guardian', which promises him protection from all harm. Subaru, the leader of the Crimson Knights, along with several other players who became acquainted with Tsukasa, set out to investigate why Tsukasa is unable to log out, and attempt to get to the bottom of the problem before it gets out of hand."
  },
}